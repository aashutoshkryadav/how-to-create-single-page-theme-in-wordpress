$(document).ready(function(){
	//Page scroll for every link at page like testimonial, services-webdesign, cms-development etc.
	$('.page a').smoothScroll({
			speed: 1400
	});	

	// Page scroll respective of navigation - smooth scroll plugin, active navigation also activated via jquery inbuilt children and addClass funtion 
	$('div#navbar li').children(':first').addClass('active');
    $('div#navbar a').smoothScroll({
		speed: 1400,
        afterScroll: function() {
            /* location.hash = this.hash; */
	$('div#navbar a.active').removeClass('active');
            $(this).addClass('active');
        }
    });

	//Back to Top : Scrolling is jquery inbuild function
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollup').fadeIn();
		} else {
			$('.scrollup').fadeOut();
		}
	}); 			
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});   
		//Preloader
		 $("body").queryLoader({
				barColor: "#213E4A",
				backgroundColor: "#355664",
				percentage: false,
				barHeight: 2,
				completeAnimation: "grow"
			}); 
	//Blog post pagination controller via ajax XHR
	$('.navigation a').live('click', function(event){ //.live( events, handler(eventObject) )
				event.preventDefault(); //Cancel the default action (navigation) of the click. So, clicked anchors will not take the browser to a new URL,
				var link = $(this).attr('href');
				var height = $('.blog-page-container').height(); //Fetch height of div
				$('.blog-page').css('min-height', height + 'px'); //Sets min height as per above div
				$('.blog-page-container').fadeOut(500).load(link + ' .blog-page-inner', function(){ jQuery('.blog-page-container').fadeIn(500); });// load() method load data from the server and place the returned HTML into the matched element. Default syntax in jQuery API is : load( url [, data] [, complete(responseText, textStatus, XMLHttpRequest)] )
			});





});//end of doc load
